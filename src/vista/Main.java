package vista;

import modelo.EquiposDAO;
import utilidades.Conexion;
import controlador.EquiposJDBC;
import modelo.Equipos;
import controlador.JugadoresJDBC;
import modelo.Jugadores;
import controlador.PartidosJDBC;
import modelo.Partidos;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://192.168.56.101/futbol?useSSL=false";
    //  Database credentials
    static final String USER = "admin";
    static final String PASS = "admin";
    public static void main(String[] args) {
        Conexion c_e = new Conexion();
        Connection conn_e = null;
        PreparedStatement stmt_e = null;

        Conexion c_j = new Conexion();
        Connection conn_j = null;
        PreparedStatement stmt_j = null;


        Conexion c_p = new Conexion();
        Connection conn_p = null;
        PreparedStatement stmt_p = null;




        try{

            System.out.println("Connecting to database...");

            System.out.println("¿Que tabla deseas utilizar?\n" +
                    "1- Equipos\n" +
                    "2- Jugadores\n" +
                    "3- Partidos\n" +
                    "4- Salir");
            Scanner sc = new Scanner(System.in);
            int tabla = sc.nextInt();
            if (tabla<4){
                int eleccion = 0;
                do {
                    switch (tabla){
                        case 1:
                            System.out.println("Has elegido la tabla Equipos, que desea hacer con ella?\n" +
                                    "1- select_all\n" +
                                    "2- select_by_id\n" +
                                    "3- select_min_fundacion\n" +
                                    "4- insert_equipo\n" +
                                    "5- delete_equipo_by_id\n" +
                                    "6- update_entrenador_by_id\n" +
                                    "7- update_all_by_id_equipo\n" +
                                    "8 u otro número- Salir");
                            eleccion = sc.nextInt();
                            switch (eleccion){
                                case 1:
                                    conn_e = c_e.getConnection();
                                    EquiposJDBC controller_e = new EquiposJDBC(conn_e);
                                    ArrayList<Equipos> lista_equipos = controller_e.selectall();
                                    for (Equipos e: lista_equipos) {
                                        System.out.println(e);
                                    }
                                    break;
                                case 2:
                                    System.out.println("Inserta el id del equipo");
                                    int id_equipo = sc.nextInt();
                                    controller_e = new EquiposJDBC(conn_e);
                                    Equipos e1 = controller_e.select_by_id(id_equipo);
                                    System.out.println(e1);
                                    break;
                                case 3:

                            }
                        case 2:
                            System.out.println("Has elegido la tabla Jugadores, que desea hacer con ella?\n" +
                                    "1- select_all\n" +
                                    "2- select_by_id\n" +
                                    "3- select_max_edad\n" +
                                    "4- insert_jugador\n" +
                                    "5- delete_jugador_by_id\n" +
                                    "6- update_jugador_by_id\n" +
                                    "7- update_all_by_id_jugador\n" +
                                    "8 u otro número- Salir");
                            eleccion = sc.nextInt();
                            switch (eleccion){
                                case 1:
                                    conn_j = c_j.getConnection();
                                    JugadoresJDBC controller_j = new JugadoresJDBC(conn_j);
                                    ArrayList<Jugadores> lista_jugadores = controller_j.selectall();
                                    for (Jugadores j : lista_jugadores) {
                                        System.out.println(j);
                                    }
                                    break;
                                case 2:
                                    System.out.println("Inserta el id del jugador");
                                    int id_jugador = sc.nextInt();
                                    controller_j = new JugadoresJDBC(conn_j);
                                    Jugadores j1 = controller_j.select_by_id(id_jugador);
                                    System.out.println(j1);
                                    break;
                                case 3:
                                    controller_j = new JugadoresJDBC(conn_j);
                                    Jugadores j2 = controller_j.select_max_edad();
                                    System.out.println(j2);
                            }
                        case 3:
                            System.out.println("Has elegido la tabla Partidos, que desea hacer con ella?\n" +
                                    "1- select_all\n" +
                                    "2- select_by_id\n" +
                                    "3- select_max_goles_local\n" +
                                    "4- insert_partido\n" +
                                    "5- delete_partido_by_id\n" +
                                    "6- update_partido_by_id\n" +
                                    "7- update_all_by_id_partido\n" +
                                    "8 u otro número- Salir");
                            eleccion = sc.nextInt();
                            switch (eleccion){
                                case 1:
                                    conn_p = c_p.getConnection();
                                    PartidosJDBC controller_p = new PartidosJDBC(conn_p);
                                    ArrayList<Partidos> lista_partidos = controller_p.selectall();
                                    for (Partidos p: lista_partidos) {
                                        System.out.println(p);
                                    }
                                    break;
                                case 2:
                                    System.out.println("Inserta el id del partido");
                                    int id_partido = sc.nextInt();
                                    controller_p = new PartidosJDBC(conn_p);
                                    Partidos p1 = controller_p.select_by_id(id_partido);
                                    System.out.println(p1);
                                    break;
                                case 3:

                            }
                    }
                }while (eleccion<8);
            }






        } catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt_e!=null)
                    stmt_e.close();

                if(stmt_j!=null)
                    stmt_j.close();

                if(stmt_p!=null)
                    stmt_p.close();

            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn_e!=null)
                    conn_e.close();

                if(conn_j!=null)
                    conn_j.close();

                if(conn_p!=null)
                    conn_p.close();

            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
    }//end main
    /*public static void main(String[] args) {
        Conexion c = new Conexion();
        Connection conn = null;
        PreparedStatement stmt = null;
        try{

            System.out.println("Connecting to database...");
            conn = c.getConnection();
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            EquiposJDBC controller_e = new EquiposJDBC(conn);
            JugadoresJDBC controller_j = new JugadoresJDBC(conn);
            PartidosJDBC controller_p = new PartidosJDBC(conn);

            ArrayList<Equipos> lista_e = controller_e.selectall();
            for (Equipos e: lista_e) {
                System.out.println(e);
            }
            ArrayList<Jugadores> lista_j = controller_j.selectall();
            for (Jugadores j: lista_j) {
                System.out.println(j);
            }
            ArrayList<Partidos> lista_p = controller_p.selectall();
            for (Partidos p: lista_p) {
                System.out.println(p);
            }

            Equipos e1 = controller_e.select_by_id(1);
            System.out.println(e1);
            Jugadores j1 = controller_j.select_by_id(1);
            System.out.println(j1);
            Partidos p1 = controller_p.select_by_id(1);
            System.out.println(p1);

            /*Equipos e1 = controller_e.select_min_fundacion();
            System.out.println(e1);
            Jugadores j1 = controller_j.select_max_edad();
            System.out.println(j1);
            Partidos p1 = controller_p.select_max_goles_local();
            System.out.println(p1);

            Equipos e1 = controller_e.insert_equipo();
            System.out.println(e1);
            Jugadores j1 = controller_j.insert_jugador();
            System.out.println(j1);
            Partidos p1 = controller_p.insert_partido();
            System.out.println(p1);

            Equipos e1 = controller_e.delete_equipo_by_id(1);
            System.out.println(e1);
            Jugadores j1 = controller_j.delete_jugador_by_id(1);
            System.out.println(j1);
            Partidos p1 = controller_p.delete_partido_by_id(1);
            System.out.println(p1);

            Equipos e1 = controller_e.update_entrenador_by_id_equipo(1, "antonio");
            System.out.println(e1);
            Jugadores j1 = controller_j.update_equipo_jugador_by_id_jugador(1, 2);
            System.out.println(j1);
            Partidos p1 = controller_p.update_fecha_by_id_partido(1, "10-10-2010");
            System.out.println(p1);
        } catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
    }//end main*/

}