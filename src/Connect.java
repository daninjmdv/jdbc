import java.sql.*;

public class Connect {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://192.168.56.101/acceso?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    //  Database credentials
    static final String USER = "admin";
    static final String PASS = "admin";
    public static void main(String[] args) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try{
            //paso 2: Register JDBC driver a partir de JDK 6 ya no hace falta
            //Class.forName(JDBC_DRIVER);

            //paso 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //paso 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.prepareStatement("select * from equipos where id_equipo like ? and nombre_equipo like ? and pais like ?");
            stmt .setString(1, "P%");
            stmt.setString(2, "%@mail.com");
            stmt.setFloat(3, 3000);

            stmt.executeUpdate();

            ResultSet rs = stmt.executeQuery();
                /*select * from user;
                where name = +___ or 1:1
                        and pass = ____; limit 1;*/

            //stmt.setFloat(1, (float) 1.1);


            while (rs.next()) {
                String name = rs.getString("name");
                int id = rs.getInt("id");
                String email = rs.getString("email");
                double salary = rs.getFloat("salary");


                System.out.print("ID: " + id);
                System.out.print(", name: " + name);
                System.out.println(", email " + email);
                System.out.println(", salary " + salary);
            }

            //STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
    }//end main

}
