package modelo;

public class Partidos {
    private int id_partido;
    private String fecha;
    private String equipo_local;
    private String equipo_visitante;
    private int goles_local;
    private int goles_visitante;
    private String estadio;
    private String arbitro;

    public Partidos(int id_partido, String fecha, String equipo_local, String equipos_visitante, int goles_local, int goles_visitante, String estadio, String arbitro) {
        this.id_partido = id_partido;
        this.fecha = fecha;
        this.equipo_local = equipo_local;
        this.equipo_visitante = equipos_visitante;
        this.goles_local = goles_local;
        this.goles_visitante = goles_visitante;
        this.estadio = estadio;
        this.arbitro = arbitro;
    }

    public int getId_partido() {
        return id_partido;
    }

    public void setId_partido(int id_partido) {
        this.id_partido = id_partido;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEquipo_local() {
        return equipo_local;
    }

    public void setEquipo_local(String equipo_local) {
        this.equipo_local = equipo_local;
    }

    public String getEquipos_visitante() {
        return equipo_visitante;
    }

    public void setEquipos_visitante(String equipos_visitante) {
        this.equipo_visitante = equipos_visitante;
    }

    public int getGoles_local() {
        return goles_local;
    }

    public void setGoles_local(int goles_local) {
        this.goles_local = goles_local;
    }

    public int getGoles_visitante() {
        return goles_visitante;
    }

    public void setGoles_visitante(int goles_visitante) {
        this.goles_visitante = goles_visitante;
    }

    public String getEstadio() {
        return estadio;
    }

    public void setEstadio(String estadio) {
        this.estadio = estadio;
    }

    public String getArbitro() {
        return arbitro;
    }

    public void setArbitro(String arbitro) {
        this.arbitro = arbitro;
    }

    @Override
    public String toString() {
        return "Partidos{" +
                "id_partido=" + id_partido +
                ", fecha='" + fecha + '\'' +
                ", equipo_local='" + equipo_local + '\'' +
                ", equipos_visitante='" + equipo_visitante + '\'' +
                ", goles_local=" + goles_local +
                ", goles_visitante=" + goles_visitante +
                ", estadio='" + estadio + '\'' +
                ", arbitro='" + arbitro + '\'' +
                '}';
    }
}
