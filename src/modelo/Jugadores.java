package modelo;

public class Jugadores {
    private int id_jugador;
    private String nombre_jugador;
    private int edad;
    private String nacionalidad;
    private String posicion;
    private int id_equipo;

    public Jugadores(int id_jugador, String nombre_jugador, int edad, String nacionalidad, String posicion, int id_equipo) {
        this.id_jugador = id_jugador;
        this.nombre_jugador = nombre_jugador;
        this.edad = edad;
        this.nacionalidad = nacionalidad;
        this.posicion = posicion;
        this.id_equipo = id_equipo;
    }

    public int getId_jugador() {
        return id_jugador;
    }

    public void setId_jugador(int id_jugador) {
        this.id_jugador = id_jugador;
    }

    public String getNombre_jugador() {
        return nombre_jugador;
    }

    public void setNombre_jugador(String nombre_jugador) {
        this.nombre_jugador = nombre_jugador;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public int getId_equipo() {
        return id_equipo;
    }

    public void setId_equipo(int id_equipo) {
        this.id_equipo = id_equipo;
    }

    @Override
    public String toString() {
        return "Jugadores{" +
                "id_jugador=" + id_jugador +
                ", nombre_jugador='" + nombre_jugador + '\'' +
                ", edad=" + edad +
                ", nacionalidad='" + nacionalidad + '\'' +
                ", posicion='" + posicion + '\'' +
                ", id_equipo=" + id_equipo +
                '}';
    }
}
