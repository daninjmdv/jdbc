package modelo;

import java.sql.SQLException;
import java.util.ArrayList;

public interface PartidosDAO {
    ArrayList<Partidos> selectall() throws SQLException;
    Partidos select_by_id(int id_partido);
    double select_max_goles_local();
    boolean insert_partido(Partidos partido);
    boolean delete_partido_by_id(int id_partido);
    boolean update_fecha_by_id_partido(int id_partido, String fecha);
    boolean update_all_by_id_equipo(String fecha, String equipo_local, String equipo_visitante, int goles_local, int goles_visitante, String estadio, String arbitro, int id_partido);
}
