package modelo;

import java.sql.SQLException;
import java.util.ArrayList;

public interface JugadoresDAO {
    ArrayList<Jugadores> selectall() throws SQLException;
    Jugadores select_by_id(int id_jugador);
    Jugadores select_max_edad();
    boolean insert_jugador(Jugadores jugador);
    boolean delete_jugador_by_id(int id_jugador);
    boolean update_equipo_jugador_by_id_jugador(int id_jugador, int id_equipo);
    boolean update_all_by_id_jugador(String nombre_jugador, int edad, String nacionalidad, String posicion, int id_equipo, int id_jugador);
}
