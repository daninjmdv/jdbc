package modelo;

import java.sql.SQLException;
import java.util.ArrayList;

public interface EquiposDAO {
    ArrayList<Equipos> selectall() throws SQLException;
    Equipos select_by_id(int id_equipo);
    double select_min_fundacion();
    boolean insert_equipo(Equipos equipos);
    boolean delete_equipo_by_id(int id_equipo);
    boolean update_entrenador_by_id_equipo(int id_equipo, String entrenador);
    boolean update_all_by_id_equipo(String nombre_equipo, String pais, String estadio, String fundacion, String entrenador, int id_equipo);
}
