package modelo;

public class Equipos {
    private int id_equipo;
    private String nombre_equipo;
    private String pais;
    private String estadio;
    private String fundacion;
    private String entrenador;

    public Equipos(int id_equipo, String nombre_equipo, String pais, String estadio, String fundacion, String entrenador) {
        this.id_equipo = id_equipo;
        this.nombre_equipo = nombre_equipo;
        this.pais = pais;
        this.estadio = estadio;
        this.fundacion = fundacion;
        this.entrenador = entrenador;
    }

    public int getId_equipo() {
        return id_equipo;
    }

    public void setId_equipo(int id_equipo) {
        this.id_equipo = id_equipo;
    }

    public String getNombre_equipo() {
        return nombre_equipo;
    }

    public void setNombre_equipo(String nombre_equipo) {
        this.nombre_equipo = nombre_equipo;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getEstadio() {
        return estadio;
    }

    public void setEstadio(String estadio) {
        this.estadio = estadio;
    }

    public String getFundacion() {
        return fundacion;
    }

    public void setFundacion(String fundacion) {
        this.fundacion = fundacion;
    }

    public String getEntrenador() {
        return entrenador;
    }

    public void setEntrenador(String entrenador) {
        this.entrenador = entrenador;
    }

    @Override
    public String toString() {
        return "Equipos{" +
                "id_equipo=" + id_equipo +
                ", nombre_equipo='" + nombre_equipo + '\'' +
                ", pais='" + pais + '\'' +
                ", estadio='" + estadio + '\'' +
                ", fundacion='" + fundacion + '\'' +
                ", entrenador='" + entrenador + '\'' +
                '}';
    }
}
