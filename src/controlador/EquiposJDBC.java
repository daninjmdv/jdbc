package controlador;

import modelo.Equipos;
import modelo.EquiposDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.*;

public class EquiposJDBC implements EquiposDAO {
    public final String SELECT_ALL = "SELECT * FROM equipos";
    public final String SELECT_BY_ID = "SELECT * FROM equipos WHERE id_equipo = ?";
    public final String SELECT_MIN_FUNDACION = "SELECT MIN(fundacion) FROM equipos";
    public final String INSERT_EQUIPOS = "INSERT equipos VALUES(?, ?, ?, ?, ?, ?)";
    public final String DELETE_EQUIPO_BY_ID = "DELETE FROM equipos WHERE id_equipo = ?";
    public final String UPDATE_ENTRENADOR_BY_ID = "UPDATE equipos SET entrenador = ? WHERE id_equipo = ?";
    public final String UPDATE_ALL_BY_ID_EQUIPO = "UPDATE equipos SET nombre_equipo = ?, pais = ?, estadio = ?, fundacion = ?, entrenador = ? WHERE id_equipo = ?";
    Connection con;
    public EquiposJDBC(Connection con){
        this.con= con;
    }
    @Override
    public ArrayList<Equipos> selectall()  {
        ArrayList<Equipos> lista_equipos = new ArrayList<Equipos>();
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                int id_equipo = rs.getInt("id_equipo");
                String nombre_equipo = rs.getString("nombre_equipo");
                String pais = rs.getString("pais");
                String estadio = rs.getString("estadio");
                String fundacion = rs.getString("fundacion");
                String entrenador = rs.getString("entrenador");
                Equipos e1 = new Equipos(id_equipo, nombre_equipo, pais, estadio, fundacion, entrenador);
                lista_equipos.add(e1);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_equipos;
    }

    @Override
    public Equipos select_by_id(int id_equipo) {
        Equipos e1 = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id_equipo);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                int id2 = rs.getInt("id_equipo");
                String nombre_equipo = rs.getString("nombre_equipo");
                String pais = rs.getString("pais");
                String estadio = rs.getString("estadio");
                String fundacion = rs.getString("fundacion");
                String entrenador = rs.getString("entrenador");

                e1 = new Equipos(id2, nombre_equipo, pais, estadio, fundacion, entrenador);

            }else{
                e1 = null;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return e1;
    }

    @Override
    public double select_min_fundacion() {
        double min_fundacion = 0;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_MIN_FUNDACION);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                min_fundacion = rs.getDouble("min_fundacion");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return min_fundacion;
    }

    @Override
    public boolean insert_equipo(Equipos equipos) {
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_EQUIPOS);
            stmt.setInt(1, equipos.getId_equipo());
            stmt.setString(2, equipos.getNombre_equipo());
            stmt.setString(3, equipos.getPais());
            stmt.setString(4, equipos.getEstadio());
            stmt.setString(5, equipos.getFundacion());
            stmt.setString(6, equipos.getEntrenador());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return false;
    }

    @Override
    public boolean delete_equipo_by_id(int id_equipo) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_EQUIPO_BY_ID);
            stmt.setInt(1, id_equipo);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_entrenador_by_id_equipo(int id_equipo, String entrenador) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_ENTRENADOR_BY_ID);
            stmt.setString(1, entrenador);
            stmt.setInt(2, id_equipo);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_all_by_id_equipo(String nombre_equipo, String pais, String estadio, String fundacion, String entrenador, int id_equipo) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_ALL_BY_ID_EQUIPO);
            stmt.setString(1, nombre_equipo);
            stmt.setString(2, pais);
            stmt.setString(3, estadio);
            stmt.setString(4, fundacion);
            stmt.setString(5, entrenador);
            stmt.setInt(6, id_equipo);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

}
