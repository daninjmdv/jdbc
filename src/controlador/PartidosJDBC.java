package controlador;

import modelo.Equipos;
import modelo.Partidos;
import modelo.PartidosDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.*;

public class PartidosJDBC implements PartidosDAO {
    public final String SELECT_ALL = "SELECT * FROM partidos";
    public final String SELECT_BY_ID = "SELECT * FROM partidos WHERE id_partido = ?";
    public final String SELECT_MAX_GOLES_LOCAL = "SELECT MAX(goles_local) FROM partidos";
    public final String INSERT_PARTIDO = "INSERT partidos VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
    public final String DELETE_PARTIDO_BY_ID = "DELETE FROM partidos WHERE id_partido = ?";
    public final String UPDATE_FECHA_BY_ID_PARTIDO = "UPDATE partidos SET fecha = ? WHERE id_partido = ?";
    public final String UPDATE_ALL_BY_ID_PARTIDO = "UPDATE partidos SET fecha = ?, equipo_local = ?, equipo_visitante = ?, goles_local = ?, goles_visitante = ?, estadio = ?, arbitro = ? WHERE id_equipo = ?";
    Connection con;
    public PartidosJDBC(Connection con){
        this.con= con;
    }
    @Override
    public ArrayList<Partidos> selectall() {
        ArrayList<Partidos> lista_partidos = new ArrayList<Partidos>();
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                int id_partido = rs.getInt("id_partido");
                String fecha = rs.getString("fecha");
                String equipo_local = rs.getString("equipo_local");
                String equipo_visitante = rs.getString("equipo_visitante");
                int goles_local = rs.getInt("goles_local");
                int goles_visitante = rs.getInt("goles_visitante");
                String estadio = rs.getString("estadio");
                String arbitro = rs.getString("arbitro");
                Partidos p1 = new Partidos(id_partido, fecha, equipo_local, equipo_visitante, goles_local, goles_visitante, estadio, arbitro);
                lista_partidos.add(p1);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_partidos;
    }

    @Override
    public Partidos select_by_id(int id_partido) {
        Partidos p1 = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id_partido);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                int id2 = rs.getInt("id_partido");
                String fecha = rs.getString("fecha");
                String equipo_local = rs.getString("equipo_local");
                String equipo_visitante = rs.getString("equipo_visitante");
                int goles_local = rs.getInt("goles_local");
                int goles_visitante = rs.getInt("goles_visitante");
                String estadio = rs.getString("estadio");
                String arbitro = rs.getString("arbitro");

                p1 = new Partidos(id2, fecha, equipo_local, equipo_visitante, goles_local, goles_visitante, estadio, arbitro);

            }else{
                p1 = null;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return p1;
    }

    @Override
    public double select_max_goles_local() {
        double max_goles_local = 0;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_MAX_GOLES_LOCAL);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                max_goles_local = rs.getDouble("max_goles_local");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return max_goles_local;
    }

    @Override
    public boolean insert_partido(Partidos partido) {
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_PARTIDO);
            stmt.setInt(1, partido.getId_partido());
            stmt.setString(2, partido.getFecha());
            stmt.setString(3, partido.getEquipo_local());
            stmt.setString(4, partido.getEquipos_visitante());
            stmt.setInt(5, partido.getGoles_local());
            stmt.setInt(6, partido.getGoles_visitante());
            stmt.setString(7, partido.getEstadio());
            stmt.setString(8, partido.getArbitro());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return false;
    }

    @Override
    public boolean delete_partido_by_id(int id_partido) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_PARTIDO_BY_ID);
            stmt.setInt(1, id_partido);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_fecha_by_id_partido(int id_partido, String fecha) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_FECHA_BY_ID_PARTIDO);
            stmt.setString(1, fecha);
            stmt.setInt(2, id_partido);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_all_by_id_equipo(String fecha, String equipo_local, String equipo_visitante, int goles_local, int goles_visitante, String estadio, String arbitro, int id_partido) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_ALL_BY_ID_PARTIDO);
            stmt.setString(1, fecha);
            stmt.setString(2, equipo_local);
            stmt.setString(3, equipo_visitante);
            stmt.setInt(4, goles_local);
            stmt.setInt(5, goles_visitante);
            stmt.setString(6, estadio);
            stmt.setString(7, arbitro);
            stmt.setInt(8, id_partido);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }
}
