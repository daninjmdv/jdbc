package controlador;

import modelo.Equipos;
import modelo.Jugadores;
import modelo.JugadoresDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.*;

public class JugadoresJDBC implements JugadoresDAO {
    public final String SELECT_ALL = "SELECT * FROM jugadores";
    public final String SELECT_BY_ID = "SELECT * FROM jugadores WHERE id_jugador = ?";
    public final String SELECT_MAX_EDAD = "SELECT MAX(edad) FROM equipos";
    public final String INSERT_JUGADORES = "INSERT jugadores VALUES(?, ?, ?, ?)";
    public final String DELETE_JUGADOR_BY_ID = "DELETE FROM jugadores WHERE id_jugador = ?";
    public final String UPDATE_EQUIPO_JUGADOR_BY_ID_JUGADOR = "UPDATE jugadores SET id_equipo = ? WHERE id_jugador = ?";
    public final String UPDATE_ALL_BY_ID_JUGADOR = "UPDATE jugadores SET nombre_jugador = ?, edad = ?, nacionalidad = ?, posicion = ?, id_equipo = ? WHERE id_jugador = ?";
    Connection con;
    public JugadoresJDBC(Connection con){
        this.con= con;
    }

    @Override
    public ArrayList<Jugadores> selectall() {
        ArrayList<Jugadores> lista_jugadores = new ArrayList<Jugadores>();
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                int id_jugador = rs.getInt("id_jugador");
                String nombre_jugador = rs.getString("nombre_jugador");
                int edad = rs.getInt("edad");
                String nacionalidad = rs.getString("nacionalidad");
                String posicion = rs.getString("posicion");
                int id_equipo = rs.getInt("id_equipo");
                Jugadores j1 = new Jugadores(id_jugador, nombre_jugador, edad, nacionalidad, posicion, id_equipo);
                lista_jugadores.add(j1);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_jugadores;
    }

    @Override
    public Jugadores select_by_id(int id_jugador) {
        Jugadores j1 = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id_jugador);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                int id2 = rs.getInt("id_jugador");
                String nombre_jugador = rs.getString("nombre_jugador");
                int edad = rs.getInt("edad");
                String nacionalidad = rs.getString("nacionalidad");
                String posicion = rs.getString("posicion");
                int id_equipo = rs.getInt("id_equipo");

                j1 = new Jugadores(id2, nombre_jugador, edad, nacionalidad, posicion, id_equipo);

            }else{
                j1 = null;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return j1;
    }

    @Override
    public Jugadores select_max_edad() {
        Jugadores j1 = null;
        double max_edad = 0;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_MAX_EDAD);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                max_edad = rs.getDouble("max_edad");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return j1;
    }

    @Override
    public boolean insert_jugador(Jugadores jugador) {
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_JUGADORES);
            stmt.setInt(1, jugador.getId_jugador());
            stmt.setString(2, jugador.getNombre_jugador());
            stmt.setInt(3, jugador.getEdad());
            stmt.setString(4, jugador.getNacionalidad());
            stmt.setString(5, jugador.getPosicion());
            stmt.setInt(6, jugador.getId_equipo());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return false;
    }

    @Override
    public boolean delete_jugador_by_id(int id_jugador) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_JUGADOR_BY_ID);
            stmt.setInt(1, id_jugador);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_equipo_jugador_by_id_jugador(int id_jugador, int id_equipo) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_EQUIPO_JUGADOR_BY_ID_JUGADOR);
            stmt.setInt(1, id_equipo);
            stmt.setInt(2, id_jugador);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }
    @Override
    public boolean update_all_by_id_jugador(String nombre_jugador, int edad, String nacionalidad, String posicion, int id_equipo, int id_jugador){
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_ALL_BY_ID_JUGADOR);
            stmt.setString(1, nombre_jugador);
            stmt.setInt(2, edad);
            stmt.setString(3, nacionalidad);
            stmt.setString(4, posicion);
            stmt.setInt(5, id_equipo);
            stmt.setInt(6, id_jugador);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

}
