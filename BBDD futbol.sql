create database futbol;
use futbol;
CREATE TABLE equipos (
    id_equipo INT AUTO_INCREMENT PRIMARY KEY,
    nombre_equipo VARCHAR(100) NOT NULL,
    pais VARCHAR(100),
    estadio VARCHAR(100),
    fundacion DATE,
    entrenador VARCHAR(100)
);
CREATE TABLE jugadores (
    id_jugador INT AUTO_INCREMENT PRIMARY KEY,
    nombre_jugador VARCHAR(100) NOT NULL,
    edad INT,
    nacionalidad VARCHAR(100),
    posicion VARCHAR(50),
    id_equipo INT,
    FOREIGN KEY (id_equipo) REFERENCES equipos(id_equipo)
);
CREATE TABLE partidos (
    id_partido INT AUTO_INCREMENT PRIMARY KEY,
    fecha DATE,
    equipo_local VARCHAR(100),
    equipo_visitante VARCHAR(100),
    goles_local INT,
    goles_visitante INT,
    estadio VARCHAR(100),
    arbitro VARCHAR(100)
);
INSERT INTO equipos (nombre_equipo, pais, estadio, fundacion, entrenador)
VALUES
    ('Real Madrid', 'España', 'Santiago Bernabéu', '1902-03-06', 'Carlo Ancelotti'),
    ('FC Barcelona', 'España', 'Camp Nou', '1899-11-29', 'Xavi Hernández'),
    ('Manchester United', 'Inglaterra', 'Old Trafford', '1878-01-01', 'Ten Hag'),
    ('Bayern Munich', 'Alemania', 'Allianz Arena', '1900-02-27', 'Julian Nagelsmann');
INSERT INTO equipos (nombre_equipo, pais, estadio, fundacion, entrenador)
VALUES
    ('Liverpool FC', 'Inglaterra', 'Anfield', '1892-03-15', 'Jürgen Klopp'),
    ('Paris Saint-Germain', 'Francia', 'Parc des Princes', '1970-08-12', 'Mauricio Pochettino');
INSERT INTO jugadores (nombre_jugador, edad, nacionalidad, posicion, id_equipo)
VALUES
    ('Lionel Messi', 35, 'Argentina', 'Delantero', 2),
    ('Cristiano Ronaldo', 38, 'Portugal', 'Delantero', 3),
    ('Kylian Mbappé', 24, 'Francia', 'Delantero', 1),
    ('Robert Lewandowski', 33, 'Polonia', 'Delantero', 4);
INSERT INTO jugadores (nombre_jugador, edad, nacionalidad, posicion, id_equipo)
VALUES
    ('Virgil van Dijk', 31, 'Países Bajos', 'Defensa', 6),
    ('Neymar Jr.', 30, 'Brasil', 'Delantero', 5);

INSERT INTO partidos (fecha, equipo_local, equipo_visitante, goles_local, goles_visitante, estadio, arbitro)
VALUES
    ('2024-03-10', 'Real Madrid', 'FC Barcelona', 2, 1, 'Santiago Bernabéu', 'Michael Oliver'),
    ('2024-03-11', 'Manchester United', 'Bayern Munich', 3, 0, 'Old Trafford', 'Michael Jordan');
INSERT INTO partidos (fecha, equipo_local, equipo_visitante, goles_local, goles_visitante, estadio, arbitro)
VALUES
    ('2024-03-15', 'Paris Saint-Germain', 'FC Barcelona', 2, 2, 'Parc des Princes', 'LeBron James');